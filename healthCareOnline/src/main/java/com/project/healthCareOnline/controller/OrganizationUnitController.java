package com.project.healthCareOnline.controller;

import com.project.healthCareOnline.dto.OrganizationUnitDto;
import com.project.healthCareOnline.entity.OrganizationUnit;
import com.project.healthCareOnline.service.OrganizationUnitService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("api/organizationUnit")
public class OrganizationUnitController {

    @Autowired
    OrganizationUnitService organizationUnitService;

    @Autowired
    ModelMapper modelMapper;

    @GetMapping(path = "/{Id}")
    @ResponseBody
    public ResponseEntity<OrganizationUnitDto> findById(@PathVariable Long Id) {
        return new ResponseEntity<>(this.convertToDto(
                organizationUnitService.findById(Id)), HttpStatus.OK);
    }

    @GetMapping(path = "/city")
    @ResponseBody
    public ResponseEntity<String[]> findAddressCities() {
        return new ResponseEntity<>(organizationUnitService.findAllCitiesOfOrgUnit(), HttpStatus.OK);
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<List<OrganizationUnitDto>> findAddressofOrgUnitByCity(@RequestParam("city") String city) {
        List<OrganizationUnit> orgUnitList = organizationUnitService.findAddressofOrgUnitByCity(city);
        List<OrganizationUnitDto> orgUnitListDto = new ArrayList<>();
        for (OrganizationUnit orgUnit : orgUnitList) {
            orgUnitListDto.add(this.convertToDto(orgUnit));
        }
        return new ResponseEntity<>(
                orgUnitListDto, HttpStatus.OK);
    }

    public OrganizationUnitDto convertToDto(OrganizationUnit organizationUnit) {
        return modelMapper.map(organizationUnit, OrganizationUnitDto.class);
    }

    public OrganizationUnit convertToEntity(OrganizationUnitDto organizationUnitDto) {
        return modelMapper.map(organizationUnitDto, OrganizationUnit.class);
    }
}
