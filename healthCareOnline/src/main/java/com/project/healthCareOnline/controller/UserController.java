package com.project.healthCareOnline.controller;

import com.project.healthCareOnline.dto.UserDto;
import com.project.healthCareOnline.entity.User;
import com.project.healthCareOnline.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/user")
public class UserController<T> {

    @Autowired
    private UserService userService;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    PasswordEncoder passwordEncoder;


    @GetMapping
    @ResponseBody
    public ResponseEntity<List<UserDto>> getUsers() {
        List<User> usersList = userService.findAll();
        if (usersList.size() > 0) {
            return ResponseEntity.ok(usersList.stream().map(this::convertToDto).collect((Collectors.toList())));
        } else {
            return ResponseEntity.noContent().build();
        }
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<?> createUsers(@Valid @RequestBody UserDto userDto) {
        User user = convertToEntity(userDto);
        userService.save(user);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    private UserDto convertToDto(User user) {
        return modelMapper.map(user, UserDto.class);
    }

    private User convertToEntity(UserDto userDto) {
        return modelMapper.map(userDto, User.class);
    }
}
