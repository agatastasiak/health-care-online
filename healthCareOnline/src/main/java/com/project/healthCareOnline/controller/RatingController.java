package com.project.healthCareOnline.controller;

import com.project.healthCareOnline.dto.RatingDtoRequest;
import com.project.healthCareOnline.dto.RatingDtoResponse;
import com.project.healthCareOnline.entity.Rating;
import com.project.healthCareOnline.repository.RatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DecimalFormat;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(path = "/api/rating")
public class RatingController {

    @Autowired
    RatingRepository ratingRepository;

    @PutMapping
    @ResponseBody
    public ResponseEntity<?> updateRating(@Valid @RequestBody RatingDtoRequest ratingDtoRequest){
        Optional<Rating> ratingData = ratingRepository.findById(ratingDtoRequest.getId());
        if(ratingData.isPresent()) {
            Rating _rating = ratingData.get();
            _rating.setGrade(ratingDtoRequest.getGrade());
            ratingRepository.save(_rating);
            RatingDtoResponse ratingDtoResponse = this.convertEntityToDto(_rating);
            return new ResponseEntity<>(ratingDtoResponse, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    private Rating convertDtoToEntity(RatingDtoRequest ratingDtoRequest){
        Rating rating = new Rating();
        rating.setId(ratingDtoRequest.getId());
        rating.setGrade(ratingDtoRequest.getGrade());
        return rating;
    }

    private RatingDtoResponse convertEntityToDto(Rating rating){
        RatingDtoResponse ratingDtoResponse = new RatingDtoResponse();
        ratingDtoResponse.setId(rating.getId());
        ratingDtoResponse.setAverageRating(rating.getAverageRating());
        return ratingDtoResponse;
    }
}
