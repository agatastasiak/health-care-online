package com.project.healthCareOnline.controller;

import com.project.healthCareOnline.dto.VaccinationDto;
import com.project.healthCareOnline.entity.Vaccination;
import com.project.healthCareOnline.service.VaccinationService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/vaccination")
public class VaccinationController {

    @Autowired
    private VaccinationService vaccinationService;

    @Autowired
    ModelMapper modelMapper;

    @GetMapping()
    public ResponseEntity<List<VaccinationDto>> getVaccinations() {
        List<Vaccination> vaccinationList = vaccinationService.findAll();
        if (vaccinationList.size() > 0) {
            List<VaccinationDto> vaccinationDtoList = new ArrayList<>();
            for (Vaccination v : vaccinationService.findAll()) {
                vaccinationDtoList.add(modelMapper.map(v, VaccinationDto.class));
            }
            return new ResponseEntity<>(vaccinationDtoList, HttpStatus.OK);
        }
        else{
            return ResponseEntity.noContent().build();
        }
    }

    @PostMapping()
    public ResponseEntity<Vaccination> addVaccination (@RequestBody @Valid VaccinationDto vaccinationDto){
        return new ResponseEntity<>(vaccinationService.save(convertToEntity(vaccinationDto)), HttpStatus.CREATED);
    }

    private Vaccination convertToEntity(VaccinationDto vaccinationDto){
        return modelMapper.map(vaccinationDto, Vaccination.class);
    }

    private VaccinationDto convertToDto(Vaccination vaccination){
        return modelMapper.map(vaccination, VaccinationDto.class);
    }
}
