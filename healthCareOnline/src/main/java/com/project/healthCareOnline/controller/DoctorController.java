package com.project.healthCareOnline.controller;

import com.project.healthCareOnline.entity.Doctor;
import com.project.healthCareOnline.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(path = "/api/doctor")
public class DoctorController {

    @Autowired
    DoctorService doctorService;

    @GetMapping()
    @ResponseBody
    public List<Doctor> getAll() {
        return doctorService.findAll();
    }

    @GetMapping(path = "specialization")
    @ResponseBody
    public Set<String> findSpecialization() {
        return doctorService.findSpecialization();
    }

    @GetMapping(path = "averageRating")
    @ResponseBody
    public double findAverageRating(@RequestParam Long id) {
        return doctorService.findAverageRatingByDoctorId(id);
    }
}
