package com.project.healthCareOnline.controller;

import com.project.healthCareOnline.dto.AddressDto;
import com.project.healthCareOnline.entity.Address;
import com.project.healthCareOnline.service.AddressService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("api/address")
public class AddressController {

    @Autowired
    AddressService addressService;

    @Autowired
    ModelMapper modelMapper;

    @GetMapping(path = "/{id}")
    public ResponseEntity<AddressDto> findById(@PathVariable Long id){
        return new ResponseEntity<>(this.convertToDto(addressService.findById(id)), HttpStatus.OK);
    }

    private AddressDto convertToDto(Address address){
        return modelMapper.map(address,AddressDto.class);
    }
}
