package com.project.healthCareOnline.controller;

import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api")
public class Home {

    @GetMapping("/home")
    @ResponseBody
    public String welcome() {
        return "Hello on my webpage";
    }
}
