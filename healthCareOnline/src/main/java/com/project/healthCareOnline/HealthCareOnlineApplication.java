package com.project.healthCareOnline;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HealthCareOnlineApplication {

	private static final Logger LOG = LoggerFactory.getLogger("Logger");

	public static void main(String[] args) {
		System.out.println("Bootstraping application...");
		SpringApplication.run(HealthCareOnlineApplication.class, args);
		System.out.println("Application has been started");
	}
}
