package com.project.healthCareOnline;

import com.project.healthCareOnline.entity.*;
import com.project.healthCareOnline.enums.Role;
import com.project.healthCareOnline.enums.Sex;
import com.project.healthCareOnline.enums.Specialization;
import com.project.healthCareOnline.service.DoctorService;
import com.project.healthCareOnline.service.RatingService;
import com.project.healthCareOnline.service.UserService;
import com.project.healthCareOnline.service.VaccinationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DbInit implements CommandLineRunner {

    @Autowired
    DoctorService doctorService;

    @Autowired
    VaccinationService vs;

    @Autowired
    UserService userService;

    @Autowired
    RatingService ratingService;

    @Autowired
    PasswordEncoder passwordEncoder;

    private Rating save;

    @Override
    public void run(String... args) {
//        this.createRatingForStart();
        this.createDoctorsForStart();
//        this.createVaccinationsForStart();
//        this.createNewUsersForStart();
    }

    @Transactional(readOnly = true)
    public void createDoctorsForStart() {
        Doctor doctors[] = {
                new Doctor("TestDoctor", "TestDoctor", "Description", Specialization.DERMATOLOGY, this.createRatingForStart()),
                new Doctor("TestDoctor", "TestDoctor", "Description", Specialization.GYNECOLOGY, this.createRatingForStart()),
                new Doctor("TestDoctor", "TestDoctor", "Description", Specialization.OTOLARYNGOLOGY, this.createRatingForStart()),
                new Doctor("TestDoctor", "TestDoctor", "Description", Specialization.DERMATOLOGY, this.createRatingForStart())
        };
        for (Doctor d : doctors
        ) {
            doctorService.save(d);
        }
        System.out.println("Doctors DbInit done");
    }

    @Transactional(readOnly = true)
    public void createVaccinationsForStart() {
        Vaccination v = new Vaccination("Vaccination1", 125.0, 50, "Globis", new OrganizationUnit("OrganizationUnit", "https://lh3.googleusercontent.com/proxy/Fl_ZYv_Hm_DX1sTdKiQjBLft4rjWkkRLQBWe-nLjQ0j4MEPEjWJCz1THEVtDiIeKf5cmHusJGc26FyPlfzy-fMdLullOIPJg5v3bIw05RXF8OcHGqw",
                new Address("Matejki", 7, 24, "20-200", "Poznań", "Poland", 52.404247, 16.900034)));
        vs.save(v);
        System.out.println("Vaccination DbInit done");
    }

    @Transactional(readOnly = true)
    public void createNewUsersForStart() {
        User[] users = {
                new User("Test1", "Test1", "Test1Email0@run.com", passwordEncoder.encode("password"), Role.NURSE, new Address("Matjki", 7, 24, "20-200", "Poznań", "Poland"), Sex.FEMALE, "90031205729", "1980-12-12"),
                new User("Test1", "Test1", "Test1Email1@run.com", passwordEncoder.encode("password"), Role.PATIENT, new Address("Gersona", 6, 1, "40-664", "Wroclaw", "Poland"), Sex.MALE, "90031105722", "1980-12-12"),
                new User("Test1", "Test1", "Test1Email2@run.com", passwordEncoder.encode("password"), Role.ADMIN, new Address("Klimta", 10, 2, "30-600", "Kraków", "Poland"), Sex.MALE, "05210550415", "1980-12-12")
        };
        for (User u : users) {
            userService.save(u);
        }
        System.out.println("Users DbInit done");
    }

    @Transactional
    public Rating createRatingForStart() {
        return ratingService.save(new Rating());
    }
}
