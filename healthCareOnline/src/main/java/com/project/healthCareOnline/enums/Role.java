package com.project.healthCareOnline.enums;

public enum Role {
	ADMIN,PATIENT,DOCTOR,NURSE,RECEPTIONIST
}
