package com.project.healthCareOnline.enums;

public enum Specialization {

    OTOLARYNGOLOGY("Otolaryngology"),
    GYNECOLOGY("Gynecology"),
    DERMATOLOGY("Dermatology"),
    PEDIATRICIAN("Pediatrician"),
    OCCUPATIONAL_HEALTH_CARE("Occupational");

    private String spec;

    Specialization(String spec) {
        this.spec = spec;
    }

    public String getSpec() {
        return spec;
    }
}
