package com.project.healthCareOnline.entity;

import com.project.healthCareOnline.enums.Role;
import com.project.healthCareOnline.enums.Sex;
import com.project.healthCareOnline.service.DateAndTimeHelpers;
import com.project.healthCareOnline.service.PeselValidation;
import com.project.healthCareOnline.service.TimeFactoryService;
import com.project.healthCareOnline.service.impl.DateAndTimeHelpersImpl;
import com.project.healthCareOnline.service.impl.TimeFactoryImpl;
import org.springframework.lang.Nullable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(updatable = false)
    private Long Id;

    @NotEmpty(message = "Field name is mandatory")
    private String name;

    @NotEmpty(message = "Field surname is mandatory")
    private String surname;

    @NotEmpty(message = "Email is manadatory")
    @Email
    @Column(unique = true)
    private String email;

    @NotBlank(message = "Password is manadatory")
    @Size(min = 3)
    private String password;

    @Enumerated(EnumType.ORDINAL)
    private Role role;

    @OneToOne(cascade = CascadeType.ALL)
    @Nullable
    @JoinColumn(name = "ADDRESS_ID_FK", referencedColumnName = "ID")
    private Address address;

    @NotNull(message = "Field is mandatory")
    @Enumerated(EnumType.ORDINAL)
    private Sex sex;

    @PeselValidation(message = "Incorrect pesel")
    @Column(unique = true)
    private String pesel;

    @Past
    private LocalDate birthDate;

    @Nullable
    @Column(nullable = false, columnDefinition = "boolean default false")
    private boolean isAdult;

    public static final DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static final TimeFactoryService timeFactoryService = new TimeFactoryImpl();
    public static final DateAndTimeHelpers helper = new DateAndTimeHelpersImpl(timeFactoryService);

    public User() {
    }

    public User(@NotEmpty(message = "Field name is mandatory") String name, @NotEmpty(message = "Field surname is mandatory") String surname, @NotEmpty(message = "Email is manadatory") @Email String email, @NotBlank(message = "Password is manadatory") @Size(min = 3) String password, Role role, @Nullable Address address, @NotNull(message = "Field is mandatory") Sex sex, String pesel, @Past String birthDate) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.role = role;
        this.address = address;
        this.sex = sex;
        this.pesel = pesel;
        this.setBirthDate(birthDate);
    }

    public Long getId() {
        return Id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public void setAdult(boolean adult) {
        isAdult = adult;
    }

    public String getPesel() {
        return pesel;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = LocalDate.parse(birthDate, User.df);
        this.setAdult();
    }

    public boolean isAdult() {
        return isAdult;
    }

    public void setAdult() {
        this.isAdult = helper.isAdult(this.getBirthDate());
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public static DateTimeFormatter getDf() { return df; }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = new BCryptPasswordEncoder().encode(password);
    }
}
