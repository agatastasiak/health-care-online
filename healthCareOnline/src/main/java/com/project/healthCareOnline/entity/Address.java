package com.project.healthCareOnline.entity;

import org.springframework.lang.Nullable;

import javax.persistence.*;

@Entity
@Table(name = "address")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String street;

    private int buildingNumber;

    private int flatNumber;

    private String zipCode;

    private String city;

    private String country;

    @Nullable
    private double latitude;

    @Nullable
    private double longitude;

    // inverse side refers to the owning side (user)
    @OneToOne(mappedBy = "address")
    private User user;

    @OneToOne(mappedBy = "address")
    private OrganizationUnit organizationUnit;

    public Address() {
    }

    public Address(String street, int buildingNumber, int flatNumber, String zipCode, String city, String country) {
        super();
        this.street = street;
        this.buildingNumber = buildingNumber;
        this.flatNumber = flatNumber;
        this.zipCode = zipCode;
        this.city = city;
        this.country = country;
    }

    public Address(String street, int buildingNumber, int flatNumber, String zipCode, String city, String country, double latitude, double longitude) {
        this.street = street;
        this.buildingNumber = buildingNumber;
        this.flatNumber = flatNumber;
        this.zipCode = zipCode;
        this.city = city;
        this.country = country;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Long getId() {
        return id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(int buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public int getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(int flatNumber) {
        this.flatNumber = flatNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
