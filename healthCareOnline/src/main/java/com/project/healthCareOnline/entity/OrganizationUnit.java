package com.project.healthCareOnline.entity;

import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.net.URL;

@Entity
public class OrganizationUnit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(updatable = false)
    private Long Id;

    private String name;

    private String imageUrl;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ADDRESS_ID_FK", referencedColumnName = "ID")
    @Nullable
    private Address address;

    public OrganizationUnit() {
    }

    public OrganizationUnit(String name, String imageUrl, @Nullable Address address) {
        this.name = name;
        this.imageUrl = imageUrl;
        this.address = address;
    }

    public OrganizationUnit(String name) {
        this.name = name;
    }

    public Long getId() {
        return Id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
