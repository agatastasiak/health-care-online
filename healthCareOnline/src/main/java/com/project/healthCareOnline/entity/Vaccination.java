package com.project.healthCareOnline.entity;

import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.Null;

@Entity
public class Vaccination {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(updatable = false)
    private Long Id;

    private String vaccinationName;
    private double price;
    private int minAge;
    private String orgUnit;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ORGANIZATION_ID_FK", referencedColumnName = "ID")
    @Nullable
    private OrganizationUnit organizationUnit;

    public Vaccination() {
    }

    public Vaccination(String vaccinationName, double price, int minAge, String orgUnit, OrganizationUnit organizationUnit) {
        this.vaccinationName = vaccinationName;
        this.price = price;
        this.minAge = minAge;
        this.orgUnit = orgUnit;
        this.organizationUnit = organizationUnit;
    }

    public Long getId() {
        return Id;
    }

    public String getVaccinationName() {
        return vaccinationName;
    }

    public void setVaccinationName(String vaccinationName) {
        this.vaccinationName = vaccinationName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getMinAge() {
        return minAge;
    }

    public void setMinAge(int minAge) {
        this.minAge = minAge;
    }

    public String getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(String orgUnit) {
        this.orgUnit = orgUnit;
    }

    public void setId(Long id) {
        Id = id;
    }

    public OrganizationUnit getOrganizationUnit() {
        return organizationUnit;
    }

    public void setOrganizationUnit(OrganizationUnit organizationUnit) {
        this.organizationUnit = organizationUnit;
    }
}
