package com.project.healthCareOnline.entity;

import com.project.healthCareOnline.enums.Specialization;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
public class Doctor {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotEmpty(message = "Field is mandatory")
    private String name;

    @NotEmpty(message = "Field is mandatory")
    private String surname;

    private String description;

    @Enumerated(EnumType.STRING)
    private Specialization specialization;

    @OneToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "rating_id_fk", referencedColumnName = "id")
    @Nullable
    private Rating rating;

    public Doctor() {
    }

    public Doctor(@NotEmpty(message = "Field is mandatory") String name, @NotEmpty(message = "Field is mandatory") String surname, String description, Specialization specialization, Rating rating) {
        this.name = name;
        this.surname = surname;
        this.description = description;
        this.specialization = specialization;
        this.rating = rating;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Specialization getSpecialization() {
        return specialization;
    }

    public void setSpecialization(Specialization specialization) {
        this.specialization = specialization;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Nullable
    public Rating getRating() {
        return rating;
    }

    public void setRating(@Nullable Rating rating) {
        this.rating = rating;
    }
}
