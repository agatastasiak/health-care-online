package com.project.healthCareOnline.entity;

import com.project.healthCareOnline.service.CalculationHelpers;
import com.sun.istack.Nullable;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;

@Entity
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Transient
    @Range(min = 0, max = 5)
    @Nullable
    private double grade;

    private double sumOfGrades;

    private double numberOfVotes;

    private double averageRating;

    @OneToOne(mappedBy = "rating")
    private Doctor doctor;

    public Rating() {
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setGrade(double grade) {
        this.grade = grade;
        this.setSumOfGrades(this.grade);
    }

    public double getGrade() {
        return grade;
    }

    private void setSumOfGrades(double grade) {
        this.sumOfGrades += grade;
        this.numberOfVotes += 1;
    }

    public double getSumOfGrades() {
        return sumOfGrades;
    }

    public double getNumberOfVotes() {
        return numberOfVotes;
    }

    public double getAverageRating() {
        return  this.averageRating;
    }

    @PrePersist
    @PreUpdate
    private void calculateAverageRating(){
        if(this.sumOfGrades > 0 && this.numberOfVotes > 0) {
            this.averageRating = CalculationHelpers.round((this.sumOfGrades/this.numberOfVotes),2);
        }
        else{
            this.averageRating = 0;
        }
    }
}
