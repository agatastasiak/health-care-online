package com.project.healthCareOnline.springSecurity.config;

import com.project.healthCareOnline.enums.Role;
import com.project.healthCareOnline.springSecurity.Authentication.UserPrincipalDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserPrincipalDetailsService userPrincipalDetailsService;

	@Bean
	PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(15);
	}

	@Bean
	DaoAuthenticationProvider daoAuthenticationProvider() {
		DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
		daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
		daoAuthenticationProvider.setUserDetailsService(this.userPrincipalDetailsService);
		return daoAuthenticationProvider;
	}

	@Override
    protected void configure(HttpSecurity http) throws Exception {
		http.httpBasic().and()
				.authorizeRequests().antMatchers(HttpMethod.GET, "/api/home").hasRole(Role.ADMIN.toString())
				.and()
				.authorizeRequests().antMatchers(HttpMethod.POST, "/api/home").hasRole(Role.ADMIN.toString())
				.and()
				.authorizeRequests().antMatchers(HttpMethod.POST, "/api/user").permitAll()
				.and()
				.authorizeRequests().antMatchers("/h2/**").permitAll()
				.and()
				.formLogin().permitAll()
				.and()
				.csrf().disable()
				.headers().frameOptions().disable();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) {
		authenticationManagerBuilder.authenticationProvider(daoAuthenticationProvider());
	}

}
