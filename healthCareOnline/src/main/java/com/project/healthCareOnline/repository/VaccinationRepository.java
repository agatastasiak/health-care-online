package com.project.healthCareOnline.repository;

import com.project.healthCareOnline.entity.Vaccination;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VaccinationRepository extends JpaRepository<Vaccination,Long> {
    Vaccination findByVaccinationName(String vaccinationName);
    Vaccination findByMinAge(int minAge);
}
