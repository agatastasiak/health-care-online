package com.project.healthCareOnline.repository;

import com.project.healthCareOnline.entity.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Long> {

    @Query("SELECT d.specialization from Doctor d")
    Set<String> findSpecialization();

    @Query("SELECT r.averageRating from Rating r INNER JOIN Doctor d ON d.rating.id = r.id where d.id=:id")
    double findAverageRatingByDoctorId(@Param("id") Long id);
}




