package com.project.healthCareOnline.repository;

import com.project.healthCareOnline.entity.OrganizationUnit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrganizationUnitRepository extends JpaRepository<OrganizationUnit,Long> {

    @Query("SELECT DISTINCT a.city FROM Address a INNER JOIN OrganizationUnit o ON a.id = o.address.id")
    String[] findAllCitiesOfOrgUnit();

    @Query(value = "SELECT o " +
            "FROM OrganizationUnit o INNER JOIN Address a ON a.id = o.address.id where a.city=:city")
    List<OrganizationUnit> findAddressofOrgUnitByCity(@Param("city") String city);
}
