package com.project.healthCareOnline.service;

import java.time.LocalDate;

public interface TimeFactoryService {

	LocalDate getCurrentDate();
}
