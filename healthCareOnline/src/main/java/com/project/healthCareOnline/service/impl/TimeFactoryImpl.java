package com.project.healthCareOnline.service.impl;

import java.time.LocalDate;

import org.springframework.stereotype.Service;

import com.project.healthCareOnline.service.TimeFactoryService;

import ch.qos.logback.core.rolling.TimeBasedFileNamingAndTriggeringPolicy;

@Service
public class TimeFactoryImpl implements TimeFactoryService {

	@Override
	public LocalDate getCurrentDate() {
		return LocalDate.now();
	}

	
}
