package com.project.healthCareOnline.service;

import com.project.healthCareOnline.entity.Rating;
import org.springframework.stereotype.Service;

@Service
public interface RatingService {

    Rating save(Rating rating);
}
