package com.project.healthCareOnline.service;

import com.project.healthCareOnline.enums.Sex;

public interface PeselValidatorService {
	
	boolean isValidPeselAllRules(String pesel, Sex sex);
}
