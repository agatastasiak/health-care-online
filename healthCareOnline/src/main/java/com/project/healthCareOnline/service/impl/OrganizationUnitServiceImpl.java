package com.project.healthCareOnline.service.impl;

import com.project.healthCareOnline.entity.OrganizationUnit;
import com.project.healthCareOnline.repository.OrganizationUnitRepository;
import com.project.healthCareOnline.service.OrganizationUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.persistence.EntityNotFoundException;

@Service
public class OrganizationUnitServiceImpl implements OrganizationUnitService {

    @Autowired
    OrganizationUnitRepository organizationUnitRepository;

    public OrganizationUnit findById(Long id) {
        return organizationUnitRepository.findById(id)
                .orElseThrow(()-> new EntityNotFoundException("Not found"));
    }

    public String[] findAllCitiesOfOrgUnit(){
        return organizationUnitRepository.findAllCitiesOfOrgUnit();
    }

    public List<OrganizationUnit> findAddressofOrgUnitByCity(String city){
        return organizationUnitRepository.findAddressofOrgUnitByCity(city);
    };


}
