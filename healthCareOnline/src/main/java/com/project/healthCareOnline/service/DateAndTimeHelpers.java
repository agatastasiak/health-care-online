package com.project.healthCareOnline.service;

import java.time.LocalDate;


public interface DateAndTimeHelpers {

	boolean isAdult(LocalDate birthDate);

}
