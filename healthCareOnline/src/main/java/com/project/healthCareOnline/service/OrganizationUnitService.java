package com.project.healthCareOnline.service;

import com.project.healthCareOnline.entity.OrganizationUnit;

import java.util.List;

public interface OrganizationUnitService {

    OrganizationUnit findById(Long id);
    String[] findAllCitiesOfOrgUnit();
    List<OrganizationUnit> findAddressofOrgUnitByCity(String city);
}
