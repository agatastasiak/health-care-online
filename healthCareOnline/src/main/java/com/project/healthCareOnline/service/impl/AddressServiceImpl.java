package com.project.healthCareOnline.service.impl;

import com.project.healthCareOnline.entity.Address;
import com.project.healthCareOnline.repository.AddressRepository;
import com.project.healthCareOnline.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    AddressRepository addressRepository;

    public Address findById(Long id){
        return addressRepository.findById(id)
                .orElseThrow(()-> new EntityNotFoundException("Id not found"));
    }
}
