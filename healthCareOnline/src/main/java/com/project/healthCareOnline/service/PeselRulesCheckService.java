package com.project.healthCareOnline.service;

import com.project.healthCareOnline.enums.Sex;

public interface PeselRulesCheckService {

	boolean isLengthCorrect(String pesel);
	boolean areAllSignsDigits(String pesel);
	boolean isSexCorrect(String pesel, Sex sex);
	boolean isControlDigitCorrect(String pesel);
	
}
