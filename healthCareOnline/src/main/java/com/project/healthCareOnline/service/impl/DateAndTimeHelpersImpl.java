package com.project.healthCareOnline.service.impl;

import java.time.LocalDate;
import java.time.Period;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.project.healthCareOnline.service.DateAndTimeHelpers;
import com.project.healthCareOnline.service.TimeFactoryService;

@Service
public class DateAndTimeHelpersImpl implements DateAndTimeHelpers {
	
	@Autowired
	private TimeFactoryService timeFactory;

	public DateAndTimeHelpersImpl() {
		super();
	}

	public DateAndTimeHelpersImpl(TimeFactoryService timeFactory) {
		super();
		this.timeFactory = timeFactory;
	}

	@Override
	public boolean isAdult(LocalDate birthDate) {
			Period p = Period.between(birthDate,timeFactory.getCurrentDate());
			return p.getYears() >= 18;
	}
}


