package com.project.healthCareOnline.service;

import com.project.healthCareOnline.entity.Vaccination;

import java.util.List;

public interface VaccinationService {

    List<Vaccination> findAll();
    Vaccination findById(Long id);
    Vaccination findByVaccinationName(String vaccinationName);
    Vaccination findByMinAge(int minAge);
    Vaccination save(Vaccination vaccination);
}
