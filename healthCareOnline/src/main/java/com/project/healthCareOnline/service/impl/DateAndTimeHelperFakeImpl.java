package com.project.healthCareOnline.service.impl;

import java.time.LocalDate;

import org.springframework.stereotype.Service;

import com.project.healthCareOnline.service.DateAndTimeHelpers;

@Service
public class DateAndTimeHelperFakeImpl implements DateAndTimeHelpers {

	@Override
	public boolean isAdult(LocalDate birthDate) {
		return false;
	}

	
}
