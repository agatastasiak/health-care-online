package com.project.healthCareOnline.service.impl;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.stereotype.Service;

import com.project.healthCareOnline.service.PeselValidation;

@Service
public class PeselValidatorServiceImpl implements ConstraintValidator<PeselValidation, String> {

	public PeselValidatorServiceImpl() {
	}
	
	@Override
	public void initialize(PeselValidation constraint) {
	}

	@Override
	public boolean isValid(String peselField, ConstraintValidatorContext ctx) {
		return peselField.length() == 11
				&& peselField.matches("^\\d{11}+$")
				&& this.isControlDigitCorrect(peselField);
	}
	
	// helper
	private int[] numericalValue(String pesel) {
		int[] p = new int[pesel.length()];
		for(int i = 0; i < pesel.length(); i++) {
			p[i] = Character.getNumericValue(pesel.charAt(i));
		}
		return p;
	} 
	
	// control digit of index 10
	// 9×a + 7×b + 3×c + 1×d + 9×e + 7×f + 3×g + 1×h + 9×i + 7×j
	private boolean isControlDigitCorrect(String pesel) {
		int p[] = this.numericalValue(pesel);
		int sum = p[0]*9 + p[1]*7 + p[2]*3 + p[3]*1 + p[4]*9 + p[5]*7 + p[6]*3 + p[7]*1 + p[8]*9 + p[9]*7;
		int lastDigit = sum % 10;
		return lastDigit == p[10];
	}
}
