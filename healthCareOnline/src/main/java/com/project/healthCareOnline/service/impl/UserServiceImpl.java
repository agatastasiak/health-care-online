package com.project.healthCareOnline.service.impl;

import com.project.healthCareOnline.entity.User;
import com.project.healthCareOnline.repository.UserRepository;
import com.project.healthCareOnline.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Transactional(propagation=Propagation.REQUIRES_NEW)
	@Override
	public User save(User user) {
		return userRepository.save(user);
	}

	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

	@Override
	public User findByName(String name) {
		return userRepository.findByName(name);
	}

	@Override
	public boolean isFound(User user) {
		return userRepository.existsById(user.getId());
	}

	@Override
	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}
}
