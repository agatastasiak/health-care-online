package com.project.healthCareOnline.service;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CalculationHelpers {

    public static double round(double value, int places){
        if(places < 0 ) throw new IllegalArgumentException("Number of decimal places must be > 0");
        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
