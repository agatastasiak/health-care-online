package com.project.healthCareOnline.service.impl;

import org.springframework.stereotype.Service;

import com.project.healthCareOnline.service.PeselRulesCheckService;
import com.project.healthCareOnline.enums.Sex;

@Service
public class PeselRulesCheckServiceImpl implements PeselRulesCheckService {
	
	//length
	public boolean isLengthCorrect(String pesel) {		
		return pesel.length() == 11;
	}
	
	//only type digits
	public boolean areAllSignsDigits(String pesel) {
		return pesel.matches("^\\d{11}+$");
	}
	
	// correct sex in index no. 10
	//  0, 2, 4, 6, 8 – female
	//  1, 3, 5, 7, 9 – male
	public boolean isSexCorrect(String pesel, Sex sex) {
		char sexDigit = pesel.charAt(9);
		if(sex.equals(Sex.FEMALE) ) {
			return sexDigit % 2 == 0;
		}
		if(sex.equals(Sex.MALE) ) {
			return sexDigit % 2 != 0;
		}
		return false;
	}
	
	private int[] numericalValue(String pesel) {
		int[] p = new int[pesel.length()];
		for(int i = 0; i < pesel.length(); i++) {
			p[i] = Character.getNumericValue(pesel.charAt(i));
		}
		return p;
	}
	
	// control digit of index 10
	// 9×a + 7×b + 3×c + 1×d + 9×e + 7×f + 3×g + 1×h + 9×i + 7×j
	public boolean isControlDigitCorrect(String pesel) {
		int p[] = this.numericalValue(pesel);
		int sum = p[0]*9 + p[1]*7 + p[2]*3 + p[3]*1 + p[4]*9 + p[5]*7 + p[6]*3 + p[7]*1 + p[8]*9 + p[9]*7;
		int lastDigit = sum % 10;
		return lastDigit == p[10];
	}

	
}
