package com.project.healthCareOnline.service;

import com.project.healthCareOnline.entity.User;

import java.util.List;

public interface UserService {

	User save(User user);

	List<User> findAll();

	User findByName(String name);

	boolean isFound(User user);

	User findByEmail(String email);
}
