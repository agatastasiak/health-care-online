package com.project.healthCareOnline.service;

import java.lang.annotation.Documented;
import java.lang.annotation.*;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.project.healthCareOnline.service.impl.PeselValidatorServiceImpl;

@Documented
@Constraint(validatedBy = {PeselValidatorServiceImpl.class})
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface PeselValidation {
	String message() default "Invalid pesel";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
