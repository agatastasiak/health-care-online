package com.project.healthCareOnline.service.impl;

import com.project.healthCareOnline.entity.Vaccination;
import com.project.healthCareOnline.repository.VaccinationRepository;
import com.project.healthCareOnline.service.VaccinationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class VaccinationServiceImpl implements VaccinationService {

    @Autowired
    VaccinationRepository vaccinationRepository;


    @Override
    public List<Vaccination> findAll() {
        return vaccinationRepository.findAll();
    }

    @Override
    public Vaccination findById(Long id) {
        return vaccinationRepository.findById(id).orElseThrow(()-> new EntityNotFoundException("Given id not found"));
    }

    @Override
    public Vaccination findByVaccinationName(String vaccinationName) {
        return vaccinationRepository.findByVaccinationName(vaccinationName);
    }

    @Override
    public Vaccination findByMinAge(int minAge) {
        return vaccinationRepository.findByMinAge(minAge);
    }

    @Override
    public Vaccination save(Vaccination vaccination) {
        return vaccinationRepository.save(vaccination);
    }
}
