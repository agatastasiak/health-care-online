package com.project.healthCareOnline.service.impl;

import com.project.healthCareOnline.entity.Rating;
import com.project.healthCareOnline.repository.RatingRepository;
import com.project.healthCareOnline.service.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RatingServiceImpl implements RatingService {

    @Autowired
    RatingRepository ratingRepository;

    @Override
    public Rating save(Rating rating) {
        return ratingRepository.save(rating);
    }

}
