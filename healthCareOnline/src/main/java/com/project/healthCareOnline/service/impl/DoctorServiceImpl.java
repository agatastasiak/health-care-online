package com.project.healthCareOnline.service.impl;

import com.project.healthCareOnline.entity.Doctor;
import com.project.healthCareOnline.repository.DoctorRepository;
import com.project.healthCareOnline.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class DoctorServiceImpl implements DoctorService {

    @Autowired
    DoctorRepository doctorRepository;

    @Override
    public List<Doctor> findAll() {
        return doctorRepository.findAll();
    }

    @Override
    public Doctor save(Doctor d) {
        return doctorRepository.save(d);
    }

    @Override
    public Set<String> findSpecialization() {
        return new HashSet<>(doctorRepository.findSpecialization());
    }

    @Override
    public double findAverageRatingByDoctorId(Long id) {
        return doctorRepository.findAverageRatingByDoctorId(id);
    }
}
