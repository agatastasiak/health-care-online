package com.project.healthCareOnline.service;

import com.project.healthCareOnline.entity.Address;

public interface AddressService {

    Address findById(Long id);
}
