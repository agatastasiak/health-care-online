package com.project.healthCareOnline.service;

import com.project.healthCareOnline.entity.Doctor;

import java.util.List;
import java.util.Set;

public interface DoctorService {

    List<Doctor> findAll();
    Doctor save(Doctor d);
    Set<String> findSpecialization();

    double findAverageRatingByDoctorId(Long id);
}
