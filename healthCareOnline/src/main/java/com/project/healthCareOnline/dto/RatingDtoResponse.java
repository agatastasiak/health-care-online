package com.project.healthCareOnline.dto;

public class RatingDtoResponse {

    private Long id;

    private double averageRating;

    public RatingDtoResponse() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(double averageRating) {
        this.averageRating = averageRating;
    }
}
