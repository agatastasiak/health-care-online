package com.project.healthCareOnline.dto;

import org.springframework.lang.Nullable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

public class OrganizationUnitDto {

    @NotEmpty(message = "Name is required")
    private String name;

    @Nullable
    private String imageUrl;

    @NotBlank
    private String addressStreet;

    @NotBlank
    private int addressBuildingNumber;

    @Nullable
    private int addressFlatNumber;

    @NotBlank
    private String addressZipCode;

    @NotBlank
    private String addressCity;

    @NotBlank
    private String addressCountry;

    private double addressLatitude;

    private double addressLongitude;

    public OrganizationUnitDto() {
    }

    public OrganizationUnitDto(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddressStreet() {
        return addressStreet;
    }

    public void setAddressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
    }

    public int getAddressBuildingNumber() {
        return addressBuildingNumber;
    }

    public void setAddressBuildingNumber(int addressBuildingNumber) {
        this.addressBuildingNumber = addressBuildingNumber;
    }

    public int getAddressFlatNumber() {
        return addressFlatNumber;
    }

    public void setAddressFlatNumber(int addressFlatNumber) {
        this.addressFlatNumber = addressFlatNumber;
    }

    public String getAddressZipCode() {
        return addressZipCode;
    }

    public void setAddressZipCode(String addressZipCode) {
        this.addressZipCode = addressZipCode;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public String getAddressCountry() {
        return addressCountry;
    }

    public void setAddressCountry(String addressCountry) {
        this.addressCountry = addressCountry;
    }

    @Nullable
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(@Nullable String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public double getAddressLatitude() {
        return addressLatitude;
    }

    public void setAddressLatitude(double addressLatitude) {
        this.addressLatitude = addressLatitude;
    }

    public double getAddressLongitude() {
        return addressLongitude;
    }

    public void setAddressLongitude(double addressLongitude) {
        this.addressLongitude = addressLongitude;
    }
}
