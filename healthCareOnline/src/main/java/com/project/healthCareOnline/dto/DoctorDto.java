package com.project.healthCareOnline.dto;

import com.project.healthCareOnline.enums.Specialization;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;

public class DoctorDto {

    @NotEmpty(message = "Field is mandatory")
    private String name;

    @NotEmpty(message = "Field is mandatory")
    private String surname;

    @Enumerated(EnumType.ORDINAL)
    private Specialization specialization;

    private String description;

    public DoctorDto() {
    }

    public DoctorDto(@NotEmpty(message = "Field is mandatory") String name, @NotEmpty(message = "Field is mandatory") String surname, Specialization specialization, String description) {
        this.name = name;
        this.surname = surname;
        this.specialization = specialization;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Specialization getSpecialization() {
        return specialization;
    }

    public void setSpecialization(Specialization specialization) {
        this.specialization = specialization;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
