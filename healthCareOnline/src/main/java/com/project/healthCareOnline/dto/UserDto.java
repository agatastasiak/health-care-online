package com.project.healthCareOnline.dto;

import com.project.healthCareOnline.enums.Role;
import com.project.healthCareOnline.enums.Sex;
import com.project.healthCareOnline.service.PeselValidation;
import org.springframework.lang.Nullable;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.*;
import java.time.LocalDate;

public class UserDto {

    @NotBlank(message = "Field name is mandatory")
    @Size(min = 1, max = 20, message = "Name must be between 2 and 20 characters long")
    private String name;
    @NotBlank(message = "Field surname is mandatory")
    @Size(min = 1, max = 20, message = "Surname must be between 2 and 20 characters long")
    private String surname;
    @NotBlank(message = "Email is manadatory")
    @Email(message = "Email is not valid")
    private String email;
    @NotBlank(message = "Password is manadatory")
    @Size(min = 3)
    private CharSequence password;
    @Enumerated(EnumType.ORDINAL)
    private Role role;
    @Nullable
    private String addressStreet;
    @Nullable
    private short addressBuildingNumber;
    @Nullable
    private short addressFlatNumber;
    @Nullable
    private String addressZipCode;
    @Nullable
    private String addressCity;
    @Nullable
    private String addressCountry;
    @NotNull(message = "Field is mandatory")
    @Enumerated(EnumType.ORDINAL)
    private Sex sex;
    @PeselValidation(message = "Pesel is not valid")
    private String pesel;
    @Past(message = "Birthdate not past")
    private LocalDate birthDate;
    @Nullable
    private boolean isAdult;

    public UserDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getAddressStreet() {
        return addressStreet;
    }

    public void setAddressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
    }

    public short getAddressBuildingNumber() {
        return addressBuildingNumber;
    }

    public int getAddressFlatNumber() {
        return addressFlatNumber;
    }

    public void setAddressFlatNumber(short addressFlatNumber) {
        this.addressFlatNumber = addressFlatNumber;
    }

    public String getAddressZipCode() {
        return addressZipCode;
    }

    public void setAddressZipCode(String addressZipCode) {
        this.addressZipCode = addressZipCode;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public String getAddressCountry() {
        return addressCountry;
    }

    public void setAddressCountry(String addressCountry) {
        this.addressCountry = addressCountry;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public boolean isAdult() {
        return isAdult;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAddressBuildingNumber(short addressBuildingNumber) {
        this.addressBuildingNumber = addressBuildingNumber;
    }

    public CharSequence getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
