package com.project.healthCareOnline.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class VaccinationDto {

    private Long Id;

    @NotBlank(message = "Name is required")
    @Size(min=1, max = 20, message = "Name must be between 2 and 20 characters long")
    private String vaccinationName;

    @NotNull(message = "Price is required")
    private double price;

    @NotNull(message = "Minimun age value is required")
    private int minAge;

    @NotBlank(message = "Organization unit is required")
    private String orgUnit;

    public VaccinationDto() {
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getVaccinationName() {
        return vaccinationName;
    }

    public void setVaccinationName(String vaccinationName) {
        this.vaccinationName = vaccinationName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getMinAge() {
        return minAge;
    }

    public void setMinAge(int minAge) {
        this.minAge = minAge;
    }

    public String getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(String orgUnit) {
        this.orgUnit = orgUnit;
    }
}
