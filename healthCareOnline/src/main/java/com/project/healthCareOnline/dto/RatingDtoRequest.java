package com.project.healthCareOnline.dto;

import com.sun.istack.NotNull;
import org.hibernate.validator.constraints.Range;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class RatingDtoRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Range(min = 1, max = 5)
    @NotNull
    private double grade;

    public RatingDtoRequest() {
    }

    public Long getId() {
        return id;
    }

    public double getGrade() {
        return grade;
    }
}
