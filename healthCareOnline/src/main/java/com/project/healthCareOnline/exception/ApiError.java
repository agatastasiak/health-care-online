package com.project.healthCareOnline.exception;

public class ApiError {

	private String message;
	private String errorCode;
	
	public ApiError(String message, String errorCode) {
		this.message = message;
		this.errorCode = errorCode;
	}
}
