package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MyFirstSeleniumTest {

    public static void main(String[] args) {

        System.setProperty("webdriver.gecko.driver", "C:\\Selenium\\geckodriver-v0.27.0-win64\\geckodriver.exe");

        String baseUrl = "http://demo.guru99.com/test/newtours/";
        String expectedTitle = "Welcome: Mercury Tours";
        String actualTitle = "";

        FirefoxDriver fd = new FirefoxDriver();
        WebDriverWait webDriverWait = new WebDriverWait(fd, 5);

        fd.get(baseUrl);
        webDriverWait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("className")));


        actualTitle = fd.getTitle();

        if (expectedTitle.contentEquals(actualTitle))
            System.out.println("Title test passed");
        else
            System.out.println("Title test failed");

        fd.close();
    }
}
