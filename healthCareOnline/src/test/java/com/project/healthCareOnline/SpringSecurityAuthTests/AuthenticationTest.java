package com.project.healthCareOnline.SpringSecurityAuthTests;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@SpringBootTest(
		webEnvironment=WebEnvironment.RANDOM_PORT,
		classes=SpringSecurityTestConfig.class
		)
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
class AuthenticationTest {
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	@WithUserDetails("testAdmin")
	public void givenAuthRequestForSecuredURLByRole_shouldSucceedWith200() throws Exception {
		
		mockMvc.perform(MockMvcRequestBuilders.get("http://localhost:" + port + "/app/users")
				.accept(MediaType.ALL))
		.andExpect(status().isOk());
	}
	
	@Test
	@WithUserDetails("testUser")
	void givenAuthRequestForSecuredURLByRole_shouldFailedWith403() throws Exception {
		
		mockMvc.perform(MockMvcRequestBuilders.get("http://localhost:" + port + "/app/users")
				.accept(MediaType.ALL))
		.andExpect(status().isForbidden());
	}
}
