package com.project.healthCareOnline.SpringSecurityAuthTests;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@TestConfiguration
public class SpringSecurityTestConfig {
	
	@Bean
	@Primary
	public UserDetailsService userDetailsServiceForTest() {
		UserDetails testAdmin = User.withDefaultPasswordEncoder()
				.username("testAdmin")
				.password("testPassword")
				.roles("ADMIN")
				.build();
		UserDetails testUser = User.withDefaultPasswordEncoder()
				.username("testUser")
				.password("testPassword2")
				.roles("TEST_ROLE")
				.build();
		
		return new InMemoryUserDetailsManager(testAdmin,testUser);
	}
}
