package com.project.healthCareOnline.peselValidationTests;

import com.project.healthCareOnline.entity.Address;
import com.project.healthCareOnline.entity.User;
import com.project.healthCareOnline.enums.Role;
import com.project.healthCareOnline.enums.Sex;
import com.project.healthCareOnline.service.PeselRulesCheckService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertAll;

@SpringBootTest
@RunWith(SpringRunner.class)
class PeselRulesCheckTests {
	
	@Autowired
	private PeselRulesCheckService peselRulesCheckService;

	private User testUserWithValidPesel;
	private User testUserWithInvalidPesel;
	private String testUserName;
	private String testUserSurname;
	private String testEmail;
	private Role testUserRole;
	private String validPesel;
	private String invalidPesel;
	private Sex sex;
	private String password;
	private Address address;
	private String birthDate;

	PeselRulesCheckTests() {}

	@BeforeEach
	public void onSetup() {

		this.testUserName = "TestUserName";
		this.testUserSurname = "TestSurname";
		this.testUserRole = Role.NURSE;
		this.validPesel = "90031205729";
		this.invalidPesel = "2/*aa77";
		this.sex = Sex.FEMALE;
		this.testEmail = "testEmailPeselRaCh";
		this.password = "password";
		this.address = new Address("Matjki", 7, 24, "20-200", "Poznań", "Poland");
		this.birthDate = "2014-04-03";
	}

	@Test
	@DisplayName("Length validation of pesel")
	public void whenLengthIsCorrect_thenPeselValid() {
		this.testUserWithValidPesel = new User(testUserName, testUserSurname, testEmail, password, testUserRole, address, sex, validPesel, birthDate);
		assertTrue(peselRulesCheckService.isLengthCorrect(testUserWithValidPesel.getPesel()));
	}
	
	@Test
	public void whenAllSignsDigits_thenPeselIsValid() {
		testUserWithValidPesel = new User(testUserName, testUserSurname, testEmail, password, testUserRole, address, sex, validPesel, birthDate);
		assertTrue(peselRulesCheckService.areAllSignsDigits(testUserWithValidPesel.getPesel()));
	}
	
	@Test
	public void whenSexIsCorrectFor10thIndex_thenPeselIsValid() {
		testUserWithValidPesel = new User(testUserName, testUserSurname, testEmail, password, testUserRole, address, sex, validPesel, birthDate);
		assertTrue(peselRulesCheckService.isSexCorrect(testUserWithValidPesel.getPesel(), testUserWithValidPesel.getSex()));
	}
	
//	@Test
//	public void whenLenghthIsIncorrect_thenPeselInvalid() {
//		testUserWithInvalidPesel = new User(testUserName,testUserSurname,testUserRole,invalidPesel,sex);
//		assertFalse(peselRulesCheckService.isLengthCorrect(testUserWithInvalidPesel.getPesel()));
//	}
//	
//	@Test
//	public void whenAllSignsAreNotDigits_thenPeselInvalid() {
//		testUserWithInvalidPesel = new User(testUserName,testUserSurname,testUserRole,invalidPesel,sex);
//		assertFalse(peselRulesCheckService.areAllSignsDigits(testUserWithInvalidPesel.getPesel()));
//	}
	
	@Test
	@DisplayName("Correct length and sex in index no. 10")
	public void whenLengthIsCorrectAndSexIsCorrectFor10thIndex_thenPeselIsInvalid() {
		testUserWithValidPesel = new User(testUserName, testUserSurname, testEmail, password, testUserRole, address, sex, validPesel, birthDate);
		assertAll("Pesel check length and sex",
				() -> {
					assertTrue("Pesel length is incorrect", peselRulesCheckService.isLengthCorrect(testUserWithValidPesel.getPesel()));
					assertTrue("User's sex in 10th digit is incorrect", peselRulesCheckService.isSexCorrect(testUserWithValidPesel.getPesel(), testUserWithValidPesel.getSex()));
				}
		);
	}
	
	@Test
	public void whenControlDigitIsCorrect_thenPeselValid() {
		testUserWithValidPesel = new User(testUserName, testUserSurname, testEmail, password, testUserRole, address, sex, validPesel, birthDate);
		assertTrue(peselRulesCheckService.isControlDigitCorrect(testUserWithValidPesel.getPesel()));
	}

}
