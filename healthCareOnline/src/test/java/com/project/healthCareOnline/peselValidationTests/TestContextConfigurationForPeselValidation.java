package com.project.healthCareOnline.peselValidationTests;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import com.project.healthCareOnline.service.DateAndTimeHelpers;
import com.project.healthCareOnline.service.impl.DateAndTimeHelperFakeImpl;

@TestConfiguration
@Profile("test")
public class TestContextConfigurationForPeselValidation {
	
	@Bean
	public DateAndTimeHelpers getDateAndTimeHelpersFake() {
		return new DateAndTimeHelperFakeImpl();
	}
}
