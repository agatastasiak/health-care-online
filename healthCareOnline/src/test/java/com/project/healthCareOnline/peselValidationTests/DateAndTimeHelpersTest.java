package com.project.healthCareOnline.peselValidationTests;


import com.project.healthCareOnline.entity.Address;
import com.project.healthCareOnline.entity.User;
import com.project.healthCareOnline.enums.Role;
import com.project.healthCareOnline.enums.Sex;
import com.project.healthCareOnline.service.DateAndTimeHelpers;
import com.project.healthCareOnline.service.TimeFactoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;

import static org.junit.Assert.assertTrue;

@SpringBootTest
class DateAndTimeHelpersTest {
	
	private User testUserWithValidPesel;
	private LocalDate current = LocalDate.of(2019, 10, 4);
	@MockBean
	private TimeFactoryService timeFactory;
	@Autowired
	@Qualifier("dateAndTimeHelpersImpl")
	private DateAndTimeHelpers helper;
	
		@BeforeEach
		public void onSetUp() {
			testUserWithValidPesel = new User("TestUserName", "TestSurname", "TestEmailAddressDaTHelpers", "password", Role.DOCTOR, new Address("Matjki", 7, 24, "20-200", "Poznań", "Poland"), Sex.FEMALE, "90031205729", "2016-06-05");
			testUserWithValidPesel.setBirthDate("1990-03-12");

			Mockito.when(timeFactory.getCurrentDate()).thenReturn(current);
		}
	
	@Test
	public void whenAgeAbove18_thenIsAdult() {
		assertTrue(helper.isAdult(testUserWithValidPesel.getBirthDate()));
	}
}
