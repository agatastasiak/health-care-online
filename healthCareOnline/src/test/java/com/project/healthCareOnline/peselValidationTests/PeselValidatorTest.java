package com.project.healthCareOnline.peselValidationTests;

import com.project.healthCareOnline.entity.Address;
import com.project.healthCareOnline.entity.User;
import com.project.healthCareOnline.enums.Role;
import com.project.healthCareOnline.enums.Sex;
import com.project.healthCareOnline.repository.UserRepository;
import com.project.healthCareOnline.service.impl.PeselValidatorServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.validation.ConstraintValidatorContext;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@SpringBootTest
class PeselValidatorTest {
	
	@MockBean
	UserRepository userRepository;
	
	private User testUser;
	private PeselValidatorServiceImpl validator = new PeselValidatorServiceImpl();
	private ConstraintValidatorContext ctx;

	@Test
	public void peselValidationPassed_whenDataCorrect() {
		testUser = new User("TestUserName", "TestSurname", "pselValidator@test.email", "password", Role.NURSE, new Address("Matjki", 7, 24, "20-200", "Poznań", "Poland"), Sex.FEMALE, "96050693529", "2000-05-05");
		Mockito.when(userRepository.save(testUser)).thenReturn(testUser);

		assertTrue(validator.isValid(testUser.getPesel(), ctx));
	}

	@Test
	public void peselValidationFailed_whenPeselLengthIncorrect() {
		testUser = new User("TestUserName", "TestSurname", "pselValidator2@test.email", "password", Role.NURSE, new Address("Matjki", 7, 24, "20-200", "Poznań", "Poland"), Sex.FEMALE, "96050627188", "2000-05-05");
		Mockito.when(userRepository.save(testUser)).thenReturn(testUser);

		assertFalse(validator.isValid(testUser.getPesel(), ctx));
	}

	@Test
	public void peselValidationFailed_whenControlDigitIncorrect() {
		testUser = new User("TestUserName", "TestSurname", "pselValidator3@test.email", "password", Role.NURSE, new Address("Matjki", 7, 24, "20-200", "Poznań", "Poland"), Sex.FEMALE, "96050627843", "2000-05-05");
		Mockito.when(userRepository.save(testUser)).thenReturn(testUser);

		assertFalse(validator.isValid(testUser.getPesel(), ctx));
	}
}
