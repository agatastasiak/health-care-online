package com.project.healthCareOnline.ControllerTests;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.gen5.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.project.healthCareOnline.controller.UserController;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
class UserControllerTests {
	
	@Autowired
	private UserController usersController;
	
	@LocalServerPort 
	private int port;
	
	@Autowired
	TestRestTemplate testRestTempalte;

	@Test
	void usersControllerTest() throws Exception {
		assertThat(usersController).isNotNull();
	}
	
	@Test
	void usersShouldReturnListOfUsers() throws Exception{
		assertThat(this.testRestTempalte.getForObject("http://localhost:" + port + "/app/users", String.class)).contains("name");
	}
	
	@Test 
	void usersControllerShouldReturnStatusCode200() {
		ResponseEntity<String> response = testRestTempalte.getForEntity("http://localhost:" + port + "/app/users", String.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	}
	
	@Test
	@DisplayName("Correct media type")
	void usersControllerShouldReturnJSONMediaType() {
		HttpHeaders httpHeader = testRestTempalte.headForHeaders("http://localhost:" + port + "/app/users");
		assertTrue(httpHeader.getContentType().includes(MediaType.APPLICATION_JSON));
	}
	
	@Test
	void homeControllerShouldReturnWelcomeMessage() {
		String message = testRestTempalte.getForObject("http://localhost:" + port + "/app/home", String.class);
		assertThat(message).contains("Hello");
	}

}
