package com.project.healthCareOnline.UserRepositoryTest;

import com.project.healthCareOnline.entity.Address;
import com.project.healthCareOnline.entity.User;
import com.project.healthCareOnline.enums.Role;
import com.project.healthCareOnline.enums.Sex;
import com.project.healthCareOnline.repository.UserRepository;
import com.project.healthCareOnline.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@Import(TestContextConfigurationForUserData.class)
@ActiveProfiles("test")
class UserServiceTest {
	
	@Autowired
	private UserService userService;
	
	@MockBean
	private UserRepository mockUserRespository;
	
	private static final Logger LOG = LoggerFactory.getLogger("JCG");
	
	private User testUser;

	@BeforeEach
	public void onSetUp() {
		testUser = new User("TestUserName", "TestSurname", "TestEmailAddressForServiceT", "password", Role.PATIENT, new Address("Matjki", 7, 24, "20-200", "Poznań", "Poland"), Sex.FEMALE, "96050631383", "1996-05-06");
		Mockito.when(mockUserRespository.findByName("TestUserName"))
				.thenReturn(testUser);

		LOG.info(testUser.getName());
	}


	@Test
	void whenValidUserName_thenFindByNameShouldFoundUser() {
		
		String name = "TestUserName";
		User found = userService.findByName(name);
		
		assertThat(found.getName())
			.isEqualTo(name);
	}

}
