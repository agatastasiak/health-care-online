package com.project.healthCareOnline.UserRepositoryTest;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import com.project.healthCareOnline.service.UserService;
import com.project.healthCareOnline.service.impl.UserServiceImpl;

@TestConfiguration
@Profile("test")
public class TestContextConfigurationForUserData {

	@Bean
	public UserService getUserService() {
		return new UserServiceImpl();
	}
}
