package com.project.healthCareOnline.UserRepositoryTest;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import com.project.healthCareOnline.entity.User;
import com.project.healthCareOnline.repository.UserRepository;

@DataJpaTest
@Import(TestContextConfigurationForUserData.class)
@ActiveProfiles("test")
class SaveUserTest {
	
	@Autowired
	private UserRepository userRepository;

	@Test
	@Sql("saveUserTest.sql")
	void whenUserSaved_thenFindByName() {
		User user = userRepository.findByName("TestName");
		assertThat(user.getName()).isEqualTo("TestName");
	}
}
